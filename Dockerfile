### Builder: Compile the program ###
FROM --platform=$BUILDPLATFORM golang:alpine as builder
ARG TARGETPLATFORM
ARG BUILDPLATFORM

# build path
RUN mkdir /app
ADD . /app/

# do de build
WORKDIR /app
RUN echo "Building for ${TARGETPLATFORM} target (${BUILDPLATFORM} builder)." && \
  CGO_ENABLED=0 GOOS=$(echo $TARGETPLATFORM| cut -d'/' -f 1) GOARCH=$(echo $TARGETPLATFORM| cut -d'/' -f 2) go build -a -installsuffix cgo -ldflags="-w -s" -o /app/prospector

### Runtime: Execute the program ###
FROM --platform=$TARGETPLATFORM scratch AS runtime

EXPOSE 2112

COPY --from=builder /app/prospector /prospector

ENTRYPOINT ["/prospector", "daemon"]
HEALTHCHECK --timeout=10s \
  CMD ["/prospector", "health"]
