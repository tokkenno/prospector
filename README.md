# Prospector
Multi-miner monitor and data recolector.

## Configuration

Mount a volume file or docker swarm configuration on `/etc/prospector/config.json`

## Retrieve collected data

- **Prometheus:** Endpoint is exposed on `/metrics` path.

## Sources

- **Storj** (v0.x and v1 APIs).
- **XMRig** (v1 API).
