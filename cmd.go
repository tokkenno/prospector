package main

import (
	"encoding/json"
	"fmt"
	"github.com/devfacet/gocmd/v3"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/robfig/cron/v3"
	"gitlab.com/tokkenno/prospector/common"
	"gitlab.com/tokkenno/prospector/sources/storj"
	"gitlab.com/tokkenno/prospector/sources/xmrig"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
)

var config common.Config

func showConfigHandle(w http.ResponseWriter, r *http.Request) {
	body, _ := json.Marshal(config)
	fmt.Fprintf(w, string(body))
}

func main() {
	argFlags := struct {
		Help        bool `short:"h" long:"help" description:"Display usage" global:"true"`
		Daemon      struct{} `command:"daemon" description:"Run in daemon mode"`
		HealthCheck struct{} `command:"health" description:"Run a health check of another instance and close"`
	}{}

	gocmd.HandleFlag("HealthCheck", func(cmd *gocmd.Cmd, args []string) error {
		healthCheck()
		return nil
	})

	gocmd.HandleFlag("Daemon", func(cmd *gocmd.Cmd, args []string) error {
		daemon()
		return nil
	})

	// Init the app
	gocmd.New(gocmd.Options{
		Name:        "prospector",
		Description: "Monitor crypto miner status",
		Version:     "1.0.0",
		Flags:       &argFlags,
		ConfigType:  gocmd.ConfigTypeAuto,
	})
}

func healthCheck() {
	config, err := common.LoadConfig()
	if err != nil {
		config.Port, _ = strconv.Atoi(os.Getenv("HEALTHCHECK_PORT"))
	}

	url := fmt.Sprintf("http://127.0.0.1:%d/health", config.Port)
	log.Println(fmt.Sprintf("Checking health of: %s", url))
	_, err = http.Get(url)
	if err != nil {
		log.Println(fmt.Sprintf("Unhealthy: %s", err.Error()))
		os.Exit(1)
	} else {
		log.Println("Healthy!!")
	}
}

func daemon() {
	config, err := common.LoadConfig()
	if err != nil {
		log.Fatalf("Error while open config file: %s", err.Error())
	}

	factories := []common.SourceFactory{
		new(storj.Factory),
		new(xmrig.Factory),
	}

	gauges := make(map[string]*prometheus.GaugeVec)

	cronScheduler := cron.New()

	if len(config.Sources) > 0 {
		for _, source := range config.Sources {
			factoryFound := false
			for _, factory := range factories {
				if source.Interval < 1 {
					// Default update interval, 15s
					source.Interval = 15
				}

				if factory.GetApiName() == source.Api {
					log.Printf("Scheduling update for %s on: %s (Each %ds)", source.Api, source.Url, source.Interval)
					monitorInstance := factory.Generate(source)

					updateSource := func() {
						measures, err := monitorInstance.Update()
						if err != nil {
							log.Println(fmt.Sprintf("Source update error: %s - %s", source.Api, source.Url))
						} else {
							for _, measure := range measures {
								gauge, exists := gauges[measure.Name]

								if !exists {
									labelNames := make([]string, len(measure.Labels))
									i := 0
									for labelName := range measure.Labels {
										labelNames[i] = labelName
										i++
									}
									gauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
										Name: measure.Name,
										Help: measure.Description,
									}, labelNames)
									gauges[measure.Name] = gauge
								}

								gauge.With(measure.Labels).Set(measure.Value)
							}
						}
					}

					_, err := cronScheduler.AddFunc(fmt.Sprintf("@every %ds", source.Interval), updateSource)
					if err != nil {
						log.Fatal(err.Error())
					} else {
						factoryFound = true
					}
					// Update the first time
					updateSource()
					break
				}
			}
			if !factoryFound {
				log.Printf("Not api of type %s found.", source.Api)
			}
		}

		cronScheduler.Start()
		defer cronScheduler.Stop()

		http.HandleFunc("/health", func(writer http.ResponseWriter, request *http.Request) {
			writer.Header().Set("Content-Type", "application/json")
			writer.Write([]byte("{ \"health\": true }"))
		})

		if config.Services.ShowConfig {
			http.HandleFunc("/config", showConfigHandle)
		}

		if config.Services.Prometheus {
			http.Handle("/metrics", promhttp.Handler())
			log.Println("Prometheus service enabled. Url: /metrics")
		}

		if config.Port < 1 || config.Port > int(math.MaxUint16) {
			config.Port = 2112
		}
		listenUri := fmt.Sprintf(":%d", config.Port)
		log.Print(fmt.Sprintf("Listening on %s", listenUri))

		http.ListenAndServe(listenUri, nil)
	} else {
		log.Fatal("The configuration file doesn't contain sources. Closing...")
	}
}
