package common

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
)

type SourceConfig struct {
	Api      string `json:"api"`
	Url      string `json:"url"`
	Name     string `json:"name"`
	Interval int    `json:"interval"`
}

type ServiceConfig struct {
	ShowConfig bool `json:"show_config"`
	Prometheus bool `json:"prometheus"`
}

type Config struct {
	Port     int            `json:"port"`
	Services ServiceConfig  `json:"services"`
	Sources  []SourceConfig `json:"sources"`
}

func LoadConfig() (Config, error) {
	var config Config
	configPath := "config.json"

	if _, err := os.Stat(configPath); err != nil {
		configPath = path.Join("/etc", "prospector", "config.json")

		if _, err := os.Stat(configPath); err != nil {
			configPath, _ = os.UserConfigDir()
			configPath = path.Join(configPath, "prospector", "config.json")

			if _, err := os.Stat(configPath); err != nil {
				return config, err
			}
		}
	}

	if configFile, err := os.Open(configPath); err == nil {
		defer configFile.Close()

		log.Println(fmt.Sprintf("Configuration file loaded: %s", configPath))

		configByteValue, _ := ioutil.ReadAll(configFile)

		err = json.Unmarshal(configByteValue, &config)

		if err != nil {
			return config, err
		} else {
			return config, nil
		}
	} else {
		log.Fatalf("Error while open config file: %s", err.Error())
		return config, err
	}
}
