package common

type SourceFactory interface {
	GetApiName() string
	Generate(config SourceConfig) Source
}

type Source interface {
	GetApiName() string
	Update() ([]SourceMeasure, error)
}

type SourceMeasure struct {
	Name        string
	Description string
	Labels      map[string]string
	Value       float64
}
