package common

type Target interface {
	GetApiName() string
	GetSource(config SourceConfig) Source
}
