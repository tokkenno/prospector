package storj

import "gitlab.com/tokkenno/prospector/common"

type Factory struct {
}

func (t Factory) GetApiName() string {
	return "storj"
}

func (t Factory) Generate(config common.SourceConfig) common.Source {
	return &Source{
		config: config,
	}
}
