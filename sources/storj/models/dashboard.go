package models

import "time"

type Satellite struct {
	ID                 string `json:"id"`
	Url                string `json:"url"`
	Disqualified       string `json:"disqualified"`
	Suspended          string `json:"suspended"`
	CurrentStorageUsed int64  `json:"currentStorageUsed"`
}

type SizeQuota struct {
	Used      float64 `json:"used"`
	Available float64 `json:"available"`
	Trash     float64 `json:"trash"`
	Overused  float64 `json:"overused"`
}

type DashboardData struct {
	NodeID         string      `json:"nodeID"`
	Wallet         string      `json:"wallet"`
	Satellites     []Satellite `json:"satellites"`
	DiskSpace      SizeQuota   `json:"diskSpace"`
	Bandwidth      SizeQuota   `json:"bandwidth"`
	LastPinged     time.Time   `json:"lastPinged"`
	Version        string      `json:"version"`
	AllowedVersion string      `json:"allowedVersion"`
	UpToDate       bool        `json:"upToDate"`
	StartedAt      time.Time   `json:"startedAt"`
	ConfiguredPort string      `json:"configuredPort"`
	QuicStatus     string      `json:"quicStatus"`
}

type DashboardRequest struct {
	Data  DashboardData `json:"data"`
	Error string        `json:"error"`
}
