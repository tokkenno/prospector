package models

type EstimatedPayout struct {
	EgressBandwidth         int64   `json:"egressBandwidth"`
	EgressBandwidthPayout   float32 `json:"egressBandwidthPayout"`
	EgressRepairAudit       int64   `json:"egressRepairAudit"`
	EgressRepairAuditPayout float32 `json:"egressRepairAuditPayout"`
	DiskSpace               float64 `json:"diskSpace"`
	DiskSpacePayout         float32 `json:"diskSpacePayout"`
	HeldRate                float32 `json:"heldRate"`
	Payout                  float64 `json:"payout"`
	Held                    float64 `json:"held"`
}

type PayoutResponse struct {
	CurrentMonth             EstimatedPayout `json:"currentMonth"`
	PreviousMonth            EstimatedPayout `json:"previousMonth"`
	CurrentMonthExpectations float32         `json:"currentMonthExpectations"`
}
