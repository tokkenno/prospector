package models

import "time"

type SatellitesData struct {
}

type SatellitesRequest struct {
	Data  SatellitesData `json:"data"`
	Error string         `json:"error"`
}

type DailyStorage struct {
	AtRestTotal   float64   `json:"atRestTotal"`
	IntervalStart time.Time `json:"intervalStart"`
}

type IngressBandwidth struct {
	Repair float64 `json:"repair"`
	Usage  float64 `json:"usage"`
}

type EgressBandwidth struct {
	Repair float64 `json:"repair"`
	Usage  float64 `json:"usage"`
	Audit  float64 `json:"audit"`
}

type DailyBandwidth struct {
	Egress        EgressBandwidth `json:"egress"`
	Ingress       EgressBandwidth `json:"ingress"`
	Delete        float64         `json:"delete"`
	IntervalStart time.Time       `json:"intervalStart"`
}

func (db *DailyBandwidth) Add(other *DailyBandwidth) {
	db.Ingress.Audit += other.Ingress.Audit
	db.Ingress.Repair += other.Ingress.Repair
	db.Ingress.Usage += other.Ingress.Usage
	db.Egress.Usage += other.Egress.Usage
	db.Egress.Repair += other.Egress.Repair
	db.Delete += other.Delete
}

type SatelliteCheck struct {
	TotalCount   float64 `json:"totalCount"`
	SuccessCount float64 `json:"successCount"`
	Alpha        float64 `json:"alpha"`
	Beta         float64 `json:"beta"`
	Score        float64 `json:"score"`
}

type SatelliteAudit struct {
	AuditScore      float32 `json:"auditScore"`
	SuspensionScore float32 `json:"suspensionScore"`
	OnlineScore     float32 `json:"onlineScore"`
	SatelliteName   string  `json:"satelliteName"`
}

type SatellitePriceModel struct {
	EgressBandwidth float32 `json:"egressBandwidth"`
	RepairBandwidth float32 `json:"repairBandwidth"`
	AuditBandwidth  float32 `json:"auditBandwidth"`
	DiskSpace       float32 `json:"diskSpace"`
}

type SatelliteData struct {
	Id                 string              `json:"id"`
	StorageDaily       []DailyStorage      `json:"storageDaily"`
	BandwidthDaily     []DailyBandwidth    `json:"bandwidthDaily"`
	StorageSummary     float64             `json:"storageSummary"`
	AverageUsageBytes  float64             `json:"averageUsageBytes"`
	BandwidthSummary   float64             `json:"bandwidthSummary"`
	EgressSummary      float64             `json:"egressSummary"`
	IngressSummary     float64             `json:"ingressSummary"`
	CurrentStorageUsed int64               `json:"currentStorageUsed"`
	Audit              SatelliteCheck      `json:"audit"`
	Audits             SatelliteAudit      `json:"audits"`
	Uptime             SatelliteCheck      `json:"uptime"`
	PriceModel         SatellitePriceModel `json:"priceModel"`
	NodeJoinedAt       time.Time           `json:"nodeJoinedAt"`
}

type SatelliteRequest struct {
	Data  SatelliteData `json:"data"`
	Error string        `json:"error"`
}
