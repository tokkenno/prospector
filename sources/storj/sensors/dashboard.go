package sensors

import (
	"gitlab.com/tokkenno/prospector/common"
	"gitlab.com/tokkenno/prospector/sources/storj/models"
	"gitlab.com/tokkenno/prospector/utils"
	"time"
)

func GetDashboardMeasures(hostId string, nodeName string, dashboardInfo *models.DashboardData) []common.SourceMeasure {
	updated := 0.0
	if dashboardInfo.UpToDate {
		updated = 1.0
	}

	commonLabels := map[string]string{"node": dashboardInfo.NodeID, "host": hostId}
	if len(nodeName) > 1 {
		commonLabels["nodeName"] = nodeName
	}

	return []common.SourceMeasure{
		{
			Name:        "storj_node_id",
			Description: "The client ID",
			Labels:      commonLabels,
			Value:       1.0,
		},
		{
			Name:        "storj_node_version",
			Description: "The client version",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"version": ""}),
			Value:       1.0,
		},
		{
			Name:        "storj_node_updated",
			Description: "The client is updated",
			Labels:      commonLabels,
			Value:       updated,
		},
		{
			Name:        "storj_node_used_diskspace",
			Description: "Storj used diskspace metrics",
			Labels:      commonLabels,
			Value:       dashboardInfo.DiskSpace.Used,
		},
		{
			Name:        "storj_node_total_diskspace",
			Description: "Storj total diskspace metrics",
			Labels:      commonLabels,
			Value:       dashboardInfo.DiskSpace.Available,
		},
		{
			Name:        "storj_node_used_bandwidth",
			Description: "Storj used bandwidth metrics",
			Labels:      commonLabels,
			Value:       dashboardInfo.Bandwidth.Used,
		},
		{
			Name:        "storj_node_total_bandwidth",
			Description: "Storj total bandwidth metrics",
			Labels:      commonLabels,
			Value:       dashboardInfo.Bandwidth.Available,
		},
		{
			Name:        "storj_node_uptime",
			Description: "Storj node uptime in seconds",
			Labels:      commonLabels,
			Value:       time.Now().Sub(dashboardInfo.StartedAt).Seconds(),
		},
	}
}
