package sensors

import (
	"gitlab.com/tokkenno/prospector/common"
	"gitlab.com/tokkenno/prospector/sources/storj/models"
)

func GetPayoutMeasures(hostId string, nodeId string, nodeName string, payoutResponse *models.PayoutResponse) []common.SourceMeasure {
	commonLabels := map[string]string{"node": nodeId, "host": hostId}
	if len(nodeName) > 1 {
		commonLabels["nodeName"] = nodeName
	}

	return []common.SourceMeasure{
		{
			Name:        "storj_payout_current_expectations",
			Description: "Current month earning expectations, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.CurrentMonthExpectations),
		},
		{
			Name:        "storj_payout_current_egress",
			Description: "Current month earning by egress bandwith, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.CurrentMonth.EgressBandwidthPayout),
		},
		{
			Name:        "storj_payout_current_repair",
			Description: "Current month earning by repair bandwith, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.CurrentMonth.EgressRepairAuditPayout),
		},
		{
			Name:        "storj_payout_current_disk",
			Description: "Current month earning by repair disk space used, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.CurrentMonth.DiskSpacePayout),
		},
		{
			Name:        "storj_payout_current_total",
			Description: "Current month earnings, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.CurrentMonth.Payout),
		},
		{
			Name:        "storj_payout_current_held_rate",
			Description: "Current month held rate.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.CurrentMonth.EgressRepairAuditPayout),
		},
		{
			Name:        "storj_payout_current_held",
			Description: "Current month held earnings, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.CurrentMonth.Payout),
		},
		{
			Name:        "storj_payout_previous_egress",
			Description: "Previous month earning by egress bandwith, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.PreviousMonth.EgressBandwidthPayout),
		},
		{
			Name:        "storj_payout_previous_repair",
			Description: "Previous month earning by repair bandwith, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.PreviousMonth.EgressRepairAuditPayout),
		},
		{
			Name:        "storj_payout_previous_disk",
			Description: "Previous month earning by repair disk space used, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.PreviousMonth.DiskSpacePayout),
		},
		{
			Name:        "storj_payout_previous_total",
			Description: "Previous month earnings, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.PreviousMonth.Payout),
		},
		{
			Name:        "storj_payout_previous_held_rate",
			Description: "Previous month held rate.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.PreviousMonth.EgressRepairAuditPayout),
		},
		{
			Name:        "storj_payout_previous_held",
			Description: "Previous month held earnings, in U.S. dollar cents.",
			Labels:      commonLabels,
			Value:       float64(payoutResponse.PreviousMonth.Payout),
		},
	}
}
