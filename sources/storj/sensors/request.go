package sensors

import (
	"gitlab.com/tokkenno/prospector/common"
	"gitlab.com/tokkenno/prospector/sources/storj/models"
)

func GetRequestMeasures(hostId string, nodeName string, nodeId string, requestInfo *models.RequestInfo) []common.SourceMeasure {
	return []common.SourceMeasure{
		{
			Name:        "storj_node_request_time",
			Description: "Duration of request to Storj node. Can be used to detect network problems.",
			Labels:      map[string]string{"node": nodeId, "host": hostId},
			Value:       float64(requestInfo.Duration.Milliseconds()),
		},
	}
}
