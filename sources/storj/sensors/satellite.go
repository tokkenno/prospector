package sensors

import (
	"gitlab.com/tokkenno/prospector/common"
	"gitlab.com/tokkenno/prospector/sources/storj/models"
	"gitlab.com/tokkenno/prospector/utils"
	"time"
)

func GetSatelliteMeasures(hostId string, nodeName string, nodeId string, satelliteId string, satelliteInfo models.SatelliteData) []common.SourceMeasure {
	monthStorage := float64(0)
	lastDayIndex := 0
	lastDayDate := time.Time{}
	for dayIndex, day := range satelliteInfo.StorageDaily {
		monthStorage += day.AtRestTotal
		if day.IntervalStart.After(lastDayDate) {
			lastDayDate = day.IntervalStart
			lastDayIndex = dayIndex
		}
	}

	monthBandwidth := new(models.DailyBandwidth)
	hasDays := false
	lastDayIndex = 0
	lastDayDate = time.Time{}
	for dayIndex, day := range satelliteInfo.BandwidthDaily {
		hasDays = true
		monthBandwidth.Add(&day)
		if day.IntervalStart.After(lastDayDate) {
			lastDayDate = day.IntervalStart
			lastDayIndex = dayIndex
		}
	}

	commonLabels := map[string]string{"node": nodeId, "host": hostId, "satellite": satelliteId}
	if len(nodeName) > 1 {
		commonLabels["nodeName"] = nodeName
	}

	measures := []common.SourceMeasure{
		{
			Name:        "storj_node_sat_summary",
			Description: "Storj satellite summary metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "storage"}),
			Value:       satelliteInfo.StorageSummary,
		},
		{
			Name:        "storj_node_sat_summary",
			Description: "Storj satellite summary metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "bandwidth"}),
			Value:       satelliteInfo.BandwidthSummary,
		},
		{
			Name:        "storj_node_sat_summary",
			Description: "Storj satellite summary metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "egress"}),
			Value:       satelliteInfo.EgressSummary,
		},
		{
			Name:        "storj_node_sat_summary",
			Description: "Storj satellite summary metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "ingress"}),
			Value:       satelliteInfo.IngressSummary,
		},
		{
			Name:        "storj_node_sat_audit",
			Description: "Storj satellite audit metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "alpha"}),
			Value:       satelliteInfo.Audit.Alpha,
		},
		{
			Name:        "storj_node_sat_audit",
			Description: "Storj satellite audit metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "beta"}),
			Value:       satelliteInfo.Audit.Beta,
		},
		{
			Name:        "storj_node_sat_audit",
			Description: "Storj satellite audit metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "score"}),
			Value:       satelliteInfo.Audit.Score,
		},
		{
			Name:        "storj_node_sat_audit",
			Description: "Storj satellite audit metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "successCount"}),
			Value:       satelliteInfo.Audit.SuccessCount,
		},
		{
			Name:        "storj_node_sat_audit",
			Description: "Storj satellite audit metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "totalCount"}),
			Value:       satelliteInfo.Audit.TotalCount,
		},

		{
			Name:        "storj_node_sat_uptime",
			Description: "Storj satellite uptime metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "alpha"}),
			Value:       satelliteInfo.Uptime.Alpha,
		},
		{
			Name:        "storj_node_sat_uptime",
			Description: "Storj satellite uptime metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "beta"}),
			Value:       satelliteInfo.Uptime.Beta,
		},
		{
			Name:        "storj_node_sat_uptime",
			Description: "Storj satellite uptime metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "score"}),
			Value:       satelliteInfo.Uptime.Score,
		},
		{
			Name:        "storj_node_sat_uptime",
			Description: "Storj satellite uptime metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "successCount"}),
			Value:       satelliteInfo.Uptime.SuccessCount,
		},
		{
			Name:        "storj_node_sat_uptime",
			Description: "Storj satellite uptime metrics",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "totalCount"}),
			Value:       satelliteInfo.Uptime.TotalCount,
		},

		{
			Name:        "storj_node_sat_month_egress",
			Description: "Storj satellite egress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "audit"}),
			Value:       monthBandwidth.Egress.Audit,
		},
		{
			Name:        "storj_node_sat_month_egress",
			Description: "Storj satellite egress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "repair"}),
			Value:       monthBandwidth.Egress.Repair,
		},
		{
			Name:        "storj_node_sat_month_egress",
			Description: "Storj satellite egress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "usage"}),
			Value:       monthBandwidth.Egress.Usage,
		},
		{
			Name:        "storj_node_sat_month_egress",
			Description: "Storj satellite egress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "repair"}),
			Value:       monthBandwidth.Egress.Repair,
		},
		{
			Name:        "storj_node_sat_month_egress",
			Description: "Storj satellite egress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "usage"}),
			Value:       monthBandwidth.Egress.Usage,
		},

		{
			Name:        "storj_node_sat_month_ingress",
			Description: "Storj satellite ingress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "audit"}),
			Value:       monthBandwidth.Ingress.Audit,
		},
		{
			Name:        "storj_node_sat_month_ingress",
			Description: "Storj satellite ingress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "repair"}),
			Value:       monthBandwidth.Ingress.Repair,
		},
		{
			Name:        "storj_node_sat_month_ingress",
			Description: "Storj satellite ingress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "usage"}),
			Value:       monthBandwidth.Ingress.Usage,
		},
		{
			Name:        "storj_node_sat_month_ingress",
			Description: "Storj satellite ingress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "repair"}),
			Value:       monthBandwidth.Ingress.Repair,
		},
		{
			Name:        "storj_node_sat_month_ingress",
			Description: "Storj satellite ingress since current month start",
			Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "usage"}),
			Value:       monthBandwidth.Ingress.Usage,
		},
	}

	if hasDays {
		measures = append(measures,
			common.SourceMeasure{
				Name:        "storj_node_sat_day_egress",
				Description: "Storj satellite egress since current day start",
				Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "audit"}),
				Value:       satelliteInfo.BandwidthDaily[lastDayIndex].Egress.Audit,
			},
			common.SourceMeasure{
				Name:        "storj_node_sat_day_egress",
				Description: "Storj satellite egress since current day start",
				Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "repair"}),
				Value:       satelliteInfo.BandwidthDaily[lastDayIndex].Egress.Repair,
			},
			common.SourceMeasure{
				Name:        "storj_node_sat_day_egress",
				Description: "Storj satellite egress since current day start",
				Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "usage"}),
				Value:       satelliteInfo.BandwidthDaily[lastDayIndex].Egress.Usage,
			},

			common.SourceMeasure{
				Name:        "storj_node_sat_day_ingress",
				Description: "Storj satellite ingress since current day start",
				Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "repair"}),
				Value:       satelliteInfo.BandwidthDaily[lastDayIndex].Ingress.Repair,
			},
			common.SourceMeasure{
				Name:        "storj_node_sat_day_ingress",
				Description: "Storj satellite ingress since current day start",
				Labels:      utils.JoinMap(commonLabels, map[string]string{"type": "usage"}),
				Value:       satelliteInfo.BandwidthDaily[lastDayIndex].Ingress.Usage,
			},

			common.SourceMeasure{
				Name:        "storj_node_sat_month_storage",
				Description: "Storj satellite data stored on disk since current month start",
				Labels:      commonLabels,
				Value:       satelliteInfo.BandwidthDaily[lastDayIndex].Ingress.Usage,
			},

			common.SourceMeasure{
				Name:        "storj_node_sat_day_storage",
				Description: "Storj satellite data stored on disk since current day start",
				Labels:      commonLabels,
				Value:       satelliteInfo.BandwidthDaily[lastDayIndex].Ingress.Usage,
			},
		)
	}

	return measures
}
