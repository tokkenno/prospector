package storj

import (
	"encoding/json"
	"fmt"
	"github.com/prometheus/common/log"
	"gitlab.com/tokkenno/prospector/common"
	"gitlab.com/tokkenno/prospector/sources/storj/models"
	"gitlab.com/tokkenno/prospector/sources/storj/sensors"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type Source struct {
	common.Source
	config common.SourceConfig
}

func (mon Source) GetApiName() string {
	return "storj"
}

func (mon Source) Update() ([]common.SourceMeasure, error) {
	start := time.Now()
	resp, err := http.Get(fmt.Sprintf("http://%s/api/sno/", mon.config.Url))
	if err != nil {
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if strings.Contains(string(body), "<!DOCTYPE html>") {
		// Fallback to pre-1.0.0 API
		return mon.UpdatePreV1(mon.config.Url)
	}

	requestInfo := new(models.RequestInfo)
	requestInfo.Duration = time.Since(start)

	var dashboard models.DashboardData
	err = json.Unmarshal(body, &dashboard)
	if err != nil {
		return nil, err
	}

	payoutResponse, err := getEstimatedPayout(mon.config.Url, dashboard.NodeID)
	if err != nil {
		return nil, err
	}

	allMeasures := make([]common.SourceMeasure, 0)

	allMeasures = append(allMeasures, sensors.GetDashboardMeasures(mon.config.Url, mon.config.Name, &dashboard)...)
	allMeasures = append(allMeasures, sensors.GetPayoutMeasures(mon.config.Url, dashboard.NodeID, mon.config.Name, payoutResponse)...)
	allMeasures = append(allMeasures, sensors.GetRequestMeasures(mon.config.Url, mon.config.Name, dashboard.NodeID, requestInfo)...)

	for _, satellite := range dashboard.Satellites {
		satMeasures, err := mon.UpdateSNOSatellite(mon.config.Url, dashboard.NodeID, satellite.ID)
		if err != nil {
			log.Warnf("The satellite %s can't be updated for node %s", satellite.ID, mon.config.Url)
		} else {
			allMeasures = append(allMeasures, satMeasures...)
		}
	}

	return allMeasures, nil
}

func getEstimatedPayout(url string, nodeId string) (*models.PayoutResponse, error) {
	resp, err := http.Get(fmt.Sprintf("http://%s/api/sno/estimated-payout", url))
	if err != nil {
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var payoutResponse models.PayoutResponse
	err = json.Unmarshal(body, &payoutResponse)
	if err != nil {
		return nil, err
	}

	return &payoutResponse, nil
}

func (mon *Source) UpdateSNOSatellite(url string, nodeId string, satelliteId string) ([]common.SourceMeasure, error) {
	start := time.Now()
	resp, err := http.Get(fmt.Sprintf("http://%s/api/sno/satellite/%s", url, satelliteId))
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	requestInfo := new(models.RequestInfo)
	requestInfo.Duration = time.Since(start)

	satellite := new(models.SatelliteData)
	err = json.Unmarshal(body, satellite)
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	return sensors.GetSatelliteMeasures(url, mon.config.Name, nodeId, satelliteId, *satellite), nil
}

func (mon *Source) UpdatePreV1(url string) ([]common.SourceMeasure, error) {
	start := time.Now()
	resp, err := http.Get(fmt.Sprintf("http://%s/api/dashboard", url))
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	requestInfo := new(models.RequestInfo)
	requestInfo.Duration = time.Since(start)

	var dashboard models.DashboardRequest
	err = json.Unmarshal(body, &dashboard)
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	allMeasures := make([]common.SourceMeasure, 0)

	allMeasures = append(allMeasures, sensors.GetDashboardMeasures(url, mon.config.Name, &dashboard.Data)...)
	allMeasures = append(allMeasures, sensors.GetRequestMeasures(url, mon.config.Name, dashboard.Data.NodeID, requestInfo)...)

	for _, satellite := range dashboard.Data.Satellites {
		satMeasures, err := mon.UpdatePreV1Satellite(url, dashboard.Data.NodeID, satellite.ID)
		if err != nil {
			log.Warnf("The satellite %s can't be updated for node %s", satellite.ID, url)
		} else {
			allMeasures = append(allMeasures, satMeasures...)
		}
	}

	return allMeasures, nil
}

func (mon *Source) UpdatePreV1Satellite(url string, nodeId string, satelliteId string) ([]common.SourceMeasure, error) {
	start := time.Now()
	resp, err := http.Get(fmt.Sprintf("http://%s/api/satellite/%s", url, satelliteId))
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	requestInfo := new(models.RequestInfo)
	requestInfo.Duration = time.Since(start)

	satellite := new(models.SatelliteRequest)
	err = json.Unmarshal(body, satellite)
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	return sensors.GetSatelliteMeasures(url, mon.config.Name, nodeId, satelliteId, satellite.Data), nil
}
