package xmrig

import "gitlab.com/tokkenno/prospector/common"

type Factory struct {
	common.SourceFactory
}

func (t Factory) GetApiName() string {
	return "xmrig"
}

func (t Factory) Generate(config common.SourceConfig) common.Source {
	return &Source{
		config: config,
	}
}