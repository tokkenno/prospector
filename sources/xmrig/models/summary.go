package models

type MemoryStatus struct {
	Free              float64 `json:"free"`
	Total             float64 `json:"total"`
	ResidentSetMemory float64 `json:"resident_set_memory"`
}

type SummaryResources struct {
	Memory              MemoryStatus `json:"memory"`
	LoadAverage         []float64    `json:"load_average"`
	HardwareConcurrency float64      `json:"hardware_concurrency"`
}

type SummaryResults struct {
	DiffCurrent float64   `json:"diff_current"`
	SharesGood  float64   `json:"shares_good"`
	SharesTotal float64   `json:"shares_total"`
	AvgTimeMs   float64   `json:"avg_time_ms"`
	HashesTotal float64   `json:"hashes_total"`
	Best        []float64 `json:"best"`
}

type SummaryHashRate struct {
	Total   []float64   `json:"total"`
	Highest float64     `json:"highest"`
	Threads [][]float64 `json:"threads"`
}

type Summary struct {
	Id          string           `json:"id"`
	WorkerId    string           `json:"worker_id"`
	Uptime      float64          `json:"uptime"`
	Resources   SummaryResources `json:"resources"`
	Features    []string         `json:"features"`
	Results     SummaryResults   `json:"results"`
	Version     string           `json:"version"`
	UserAgent   string           `json:"ua"`
	DonateLevel float64          `json:"donate_level"`
	Algorithms  []string         `json:"algorithms"`
	HashRate    SummaryHashRate  `json:"hashrate"`
	HugePages   bool             `json:"hugepages"`
}
