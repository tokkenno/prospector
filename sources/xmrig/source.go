package xmrig

import (
	"encoding/json"
	"fmt"
	"gitlab.com/tokkenno/prospector/common"
	"gitlab.com/tokkenno/prospector/sources/xmrig/models"
	"io/ioutil"
	"net/http"
	"time"
)

type Source struct {
	common.Source
	config common.SourceConfig
}

func (mon Source) GetApiName() string {
	return "xmrig"
}

func (mon Source) Update() ([]common.SourceMeasure, error) {
	start := time.Now()
	resp, err := http.Get(fmt.Sprintf("http://%s/1/summary", mon.config.Url))
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	requestDuration := time.Since(start)

	var summary models.Summary
	err = json.Unmarshal(body, &summary)
	if err != nil {
		return make([]common.SourceMeasure, 0), err
	}

	measures := []common.SourceMeasure{
		{
			Name:        mon.GetApiName() + "_node",
			Description: "Node information.",
			Labels:      map[string]string{"host": mon.config.Url, "version": summary.Version, "user_agent": summary.UserAgent},
			Value:       1.0,
		},
		{
			Name:        mon.GetApiName() + "_request_time",
			Description: "Duration of request to XMRig node. Can be used to detect network problems.",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       float64(requestDuration.Milliseconds()),
		},
		{
			Name:        mon.GetApiName() + "_uptime",
			Description: "Miner uptime (seconds)",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Uptime,
		},
		{
			Name:        mon.GetApiName() + "_huge_pages",
			Description: "Huge pages is enabled",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       map[bool]float64{false: 0.0, true: 1.0}[summary.HugePages],
		},
		{
			Name:        mon.GetApiName() + "_donation_level",
			Description: "Miner donation level",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.DonateLevel,
		},
		{
			Name:        mon.GetApiName() + "_resources_memory_free",
			Description: "System memory free",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Resources.Memory.Free,
		},
		{
			Name:        mon.GetApiName() + "_resources_memory_total",
			Description: "System memory total",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Resources.Memory.Total,
		},
		{
			Name:        mon.GetApiName() + "_resources_hardware_concurrency",
			Description: "Number of hardware cores",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Resources.HardwareConcurrency,
		},
		{
			Name:        mon.GetApiName() + "_miner_difficult",
			Description: "Current job difficult",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Results.DiffCurrent,
		},
		{
			Name:        mon.GetApiName() + "_miner_shares_good",
			Description: "Miner good shares",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Results.SharesGood,
		},
		{
			Name:        mon.GetApiName() + "_miner_shares_bad",
			Description: "Miner bad shares",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Results.SharesTotal - summary.Results.SharesGood,
		},
		{
			Name:        mon.GetApiName() + "_miner_hashes_total",
			Description: "Miner hashes processed",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Results.HashesTotal,
		},
		{
			Name:        mon.GetApiName() + "_miner_avg_time",
			Description: "Average job time in milliseconds",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Results.AvgTimeMs,
		},
	}

	if len(summary.Resources.LoadAverage) > 0 && summary.Resources.HardwareConcurrency != 0 {
		measures = append(measures, common.SourceMeasure{
			Name:        mon.GetApiName() + "_resources_load",
			Description: "Cpu load",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.Resources.LoadAverage[0] / summary.Resources.HardwareConcurrency,
		})
	}

	if len(summary.HashRate.Total) > 0 {
		measures = append(measures, common.SourceMeasure{
			Name:        mon.GetApiName() + "_miner_hashes_rate",
			Description: "Miner current hash rate",
			Labels:      map[string]string{"host": mon.config.Url},
			Value:       summary.HashRate.Total[0],
		})
	}

	return measures, nil
}
