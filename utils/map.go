package utils

func JoinMap[T comparable, U any](m1 map[T]U, m2 map[T]U) map[T]U {
	mjoin := make(map[T]U)
	for k, v := range m2 {
		mjoin[k] = v
	}
	for k, v := range m1 {
		mjoin[k] = v
	}
	return mjoin
}
